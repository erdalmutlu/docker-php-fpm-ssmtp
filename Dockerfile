FROM php:fpm
LABEL maintainer="Erdal MUTLU erdal@sisiya.org"

# Install sSMTP for mail support
RUN apt-get update \
	&& apt-get install -y -q --no-install-recommends \
		ssmtp \
	&& apt-get clean \
	&& rm -r /var/lib/apt/lists/*

# the image defaults are OK for me
