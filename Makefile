all: build push

# build and tag
build:
	docker build --tag erdalmutlu/php-fpm-ssmtp:latest .
	docker tag erdalmutlu/php-fpm-ssmtp:latest erdalmutlu/php-fpm-ssmtp

#push to docker
push:
	docker push erdalmutlu/php-fpm-ssmtp
