# docker-php-fpm-ssmtp

php-fpm with ssmtp mail support

## Configure sSMTP
Change the following according to your needs:
```bash
FromLineOverride=YES
mailhub=mail.example.org
hostname=server1.example.org
UseTLS=NO
UseSTARTTLS=NO
```

And save the file as `ssmtp.conf`

## Configure mail for php:fpm
We need to set the `sendmail_path` to use the sSMTP
```bash
[mail function]
sendmail_path = "/usr/sbin/ssmtp -t"
```

And save the file as `mail.ini`

## Putting it all together
To run the following needs to be mounted:

- /path/to/ssmtp.conf:/etc/ssmtp/ssmtp.conf:ro
- /path/to/mail.ini:/usr/local/etc/php/conf.d/mail.ini:ro
- /path/to/html_scripts:/usr/share/nginx/html:ro

Clone the repo or copy and the `docker-compose.yml` and run
```bash
docker-compose up -d
```
